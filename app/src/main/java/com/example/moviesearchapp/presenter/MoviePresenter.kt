package com.example.moviesearchapp.presenter

import com.example.moviesearchapp.model.MovieModel
import com.example.moviesearchapp.MovieActivity
import com.example.moviesearchapp.model.MovieDetailModel
import com.example.moviesearchapp.service.MovieApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MoviePresenter(private val view: MovieActivity, private val sevice: MovieApiService) {

    var totalPage = 0

    fun getMovieApi(query: String?, page: Int) {

        if (query != null) {
            if (page <= totalPage || page == 1) {
                sevice.getMovie(query, page).enqueue(object : Callback<MovieModel> {
                    override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<MovieModel>,
                        response: Response<MovieModel>
                    ) {
                        val model: MovieModel? = response.body()
                        if (model != null) {
                            totalPage = model.totalPage
                        }
                        model?.result?.apply {
                            if (this.isNotEmpty()) {
                                view.setAdapter(this)
                                println(this)
                            }
                        }

                    }

                })
            }
        }
    }


}



