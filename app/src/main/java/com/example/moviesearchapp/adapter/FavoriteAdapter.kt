package com.example.moviesearchapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moviesearchapp.model.MovieFavoriteModel
import com.example.moviesearchapp.R

class FavoriteAdapter(
    private val listener: FavClickListener
    , private var favList: ArrayList<MovieFavoriteModel>
) : RecyclerView.Adapter<FavListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavListViewHolder {
        return FavListViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return favList.count()
    }

    override fun onBindViewHolder(holder: FavListViewHolder, position: Int) {
        holder.bind(favList[position], listener)
    }

}

class FavListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.list_fav, parent, false)
) {

    private val imagePoster: ImageView = itemView.findViewById(R.id.im_poster)
    private val textName: TextView = itemView.findViewById(R.id.tv_name)
    private val textDate: TextView = itemView.findViewById(R.id.tv_date)
    private val textOverview: TextView = itemView.findViewById(R.id.tv_overview)
    private var voteMovie: Double = 0.00
    private var idMovie: Int = 0
    private var posterURL: String = ""


    fun bind(model: MovieFavoriteModel, listener: FavClickListener) {

        itemView.setOnClickListener {
            listener.onItemClick(model)
        }

        val requestOptions = RequestOptions
            .errorOf(R.drawable.icon_error)
        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(model.movieImage)
            .into(imagePoster)

        textName.text = model.movieName
        textDate.text = model.movieDate
        textOverview.text = model.movieOverview
        voteMovie = model.movieVote
        idMovie = model.idMovie
        posterURL = model.movieImage

        itemView.visibility = if (model.isComplete) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}


interface FavClickListener {

    fun onItemClick(modelMovie: MovieFavoriteModel)
}



