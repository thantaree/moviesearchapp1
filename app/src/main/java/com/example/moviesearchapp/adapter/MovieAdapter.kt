package com.example.moviesearchapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moviesearchapp.R
import com.example.moviesearchapp.model.MovieDetailModel

class MovieAdapter(
    private val listener: MovieClickListener
) : RecyclerView.Adapter<MovieListViewHolder>() {


    private var movieList: ArrayList<MovieDetailModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        return MovieListViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return movieList.count()
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        holder.bind(movieList[position], listener)
    }


    fun addList(modelMovieList: ArrayList<MovieDetailModel>) {
        movieList.addAll(modelMovieList)
        notifyDataSetChanged()
    }
}


class MovieListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
) {

    private val imagePoster: ImageView = itemView.findViewById(R.id.im_poster)
    private val textName: TextView = itemView.findViewById(R.id.tv_name)
    private val textDate: TextView = itemView.findViewById(R.id.tv_date)
    private val textOverview: TextView = itemView.findViewById(R.id.tv_overview)
    private var voteMovie: Double = 0.00
    private var idMovie: Int = 0

    val baseURL: String = "https://image.tmdb.org/t/p/w92"


    fun bind(model: MovieDetailModel, listener: MovieClickListener) {

        val requestOptions = RequestOptions
            .errorOf(R.drawable.icon_error)
        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(baseURL + model.image)
            .into(imagePoster)

        textName.text = model.name
        textDate.text = model.date
        textOverview.text = model.overview
        voteMovie = model.voteavg
        idMovie = model.id


        itemView.setOnClickListener {
            listener.onItemClick(

                baseURL + model.image,
                textName.text.toString(),
                voteMovie,
                textOverview.text.toString(),
                textDate.text.toString(),
                idMovie

            )
        }
    }
}


interface MovieClickListener {

    fun onItemClick(
        imageURL: String,
        textName: String,
        vote: Double,
        textOverview: String,
        textDate: String,
        idMovie: Int
    )
}



