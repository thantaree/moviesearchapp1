package com.example.moviesearchapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesearchapp.adapter.FavClickListener
import com.example.moviesearchapp.adapter.FavoriteAdapter
import com.example.moviesearchapp.databaseFavorite.DatabaseHandler
import com.example.moviesearchapp.`interface`.FavoriteMovieInterface
import com.example.moviesearchapp.model.MovieFavoriteModel
import kotlinx.android.synthetic.main.activity_favorite.*


class FavoriteActivity : AppCompatActivity(), FavoriteMovieInterface {

    private lateinit var favoriteAdapter: FavoriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)

        setFavList()

    }
    fun setFavList(){
        val db = DatabaseHandler(applicationContext)

        val favList: ArrayList<MovieFavoriteModel> = arrayListOf()
        val data = db.readAllMovie()

        favList.addAll(data)
        setAdapterFav(favList)
    }

    override fun onResume() {
        setFavList()

        super.onResume()
    }

    override fun setAdapterFav(modelFavMovieList: ArrayList<MovieFavoriteModel>) {

        val listener = object : FavClickListener {
            override fun onItemClick(modelMovie: MovieFavoriteModel) {

                val intent =
                    Intent(this@FavoriteActivity, ShowDetailActivity::class.java).apply {
                        putExtra("imageMovie", modelMovie.movieImage)
                        putExtra("textName", modelMovie.movieName)
                        putExtra("vote", modelMovie.movieVote)
                        putExtra("textOverview", modelMovie.movieOverview)
                        putExtra("textDate", modelMovie.movieDate)
                        putExtra("idMovie", modelMovie.idMovie)
                    }
                startActivity(intent)

            }
        }

        favoriteAdapter = FavoriteAdapter(
            listener,
            modelFavMovieList
        )

        rvFavorite.adapter = favoriteAdapter
        rvFavorite.layoutManager = LinearLayoutManager(this)
        rvFavorite.itemAnimator = DefaultItemAnimator()

    }


}
