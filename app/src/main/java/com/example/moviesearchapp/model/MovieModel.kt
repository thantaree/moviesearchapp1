package com.example.moviesearchapp.model

import com.google.gson.annotations.SerializedName

data class MovieModel(

    @SerializedName("results")
    var result: ArrayList<MovieDetailModel>?,

    @SerializedName("total_pages")
    var totalPage : Int

)

class MovieDetailModel(

    @SerializedName("id")
    var id: Int,

    @SerializedName("poster_path")
    var image: String?,

    @SerializedName("original_title")
    var name: String?,

    @SerializedName("release_date")
    var date: String?,

    @SerializedName("overview")
    var overview: String?,

    @SerializedName("vote_average")
    var voteavg: Double


)
