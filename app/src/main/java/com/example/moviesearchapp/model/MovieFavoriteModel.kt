package com.example.moviesearchapp.model


class MovieFavoriteModel {

    var isComplete: Boolean = true
    var idMovie: Int = 0
    var movieName: String = ""
    var movieDate: String = ""
    var movieOverview: String = ""
    var movieImage: String = ""
    var movieVote: Double = 0.00

    constructor(
        idMovie: Int, movieName: String,
        movieDate: String, movieOverview: String, movieImage: String, movieVote: Double
    ) {
        this.idMovie = idMovie
        this.movieName = movieName
        this.movieDate = movieDate
        this.movieOverview = movieOverview
        this.movieImage = movieImage
        this.movieVote = movieVote
    }

    constructor() {
        this.idMovie = idMovie
        this.movieName = movieName
        this.movieDate = movieDate
        this.movieOverview = movieOverview
        this.movieImage = movieImage
        this.movieVote = movieVote
    }
}