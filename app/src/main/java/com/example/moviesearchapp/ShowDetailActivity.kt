package com.example.moviesearchapp

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.moviesearchapp.databaseFavorite.DatabaseHandler
import com.example.moviesearchapp.`interface`.ShowDetailInterface
import com.example.moviesearchapp.model.MovieFavoriteModel
import kotlinx.android.synthetic.main.activity_detail.*

class ShowDetailActivity : AppCompatActivity(), ShowDetailInterface {

    var isStatus: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val db = DatabaseHandler(applicationContext)

        val checkList: ArrayList<MovieFavoriteModel> = arrayListOf()
        val data = db.readAllMovie()
        checkList.addAll(data)

        val idCheckMovie = intent.getIntExtra("idMovie", 0)
        checkFavorite(idCheckMovie, checkList)

        setDetailMovie()
        addFavoriteMovie(checkList)
        actionBar()


    }
    fun checkFavorite(idCheck: Int, list: MutableList<MovieFavoriteModel>) {

        val isStatus = list.any { it.idMovie == idCheck }

        if (isStatus) {
            btn_favorite.text = getString(R.string.btn_unfavorite)
        }
    }

    fun actionBar() {
        val actionbar = supportActionBar
        actionbar?.let {
            title = ""
            actionbar.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun setDetailMovie() {

        val imageURL = intent.getStringExtra("imageMovie")
        val textName = intent.getStringExtra("textName")
        val vote = intent.getDoubleExtra("vote", 0.00)
        val textOverview = intent.getStringExtra("textOverview")

        val imageDetail: ImageView = findViewById(R.id.im_poster_detaill)
        val titleMovie: TextView = findViewById(R.id.tv_title)
        val textVote: TextView = findViewById(R.id.tv_vote)
        val textDesceiption: TextView = findViewById(R.id.tv_description)


        Glide.with(this)
            .load(imageURL)
            .into(imageDetail)

        titleMovie.text = textName
        textVote.text = String.format(getString(R.string.AverageVote) + vote.toString())
        textDesceiption.text = textOverview
    }

    override fun addFavoriteMovie(list: MutableList<MovieFavoriteModel>) {

        val imageURL = intent.getStringExtra("imageMovie") ?: ""
        val textName = intent.getStringExtra("textName") ?: ""
        val textDate = intent.getStringExtra("textDate") ?: ""
        val textOverview = intent.getStringExtra("textOverview") ?: ""
        val idMovie = intent.getIntExtra("idMovie", 0)
        val vote = intent.getDoubleExtra("vote", 0.00)

        val buttonFav = findViewById<Button>(R.id.btn_favorite)


        val context = this
        buttonFav.setOnClickListener {

            val db = DatabaseHandler(context)

            isStatus = list.any { it.idMovie == idMovie }

            if (!isStatus) {

                buttonFav.text = getString(R.string.btn_unfavorite)

                val favorite = MovieFavoriteModel(
                    idMovie.toString().toInt(),
                    textName,
                    textDate,
                    textOverview,
                    imageURL,
                    vote.toString().toDouble()
                )
                db.insertFavMovie(favorite)

            } else {

                buttonFav.text = getString(R.string.btn_favorite)
                db.deleteFavMovie(idMovie)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menubacktosearch, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id: Int = item.itemId
        if (id == R.id.menu_backtosearch) {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

            return true
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}




