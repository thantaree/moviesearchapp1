package com.example.moviesearchapp.databaseFavorite

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.moviesearchapp.model.MovieFavoriteModel


class DatabaseHandler(var context: Context) : SQLiteOpenHelper(
    context, DATABASE_NAME,
    null, 1
) {

    companion object {

        const val DATABASE_NAME = "MovieDatabase"
        const val TABLE_NAME = "FavoriteMovie"

        const val COL_IDMOVIE = "id"
        const val COL_NAME = "name"
        const val COL_DATE = "date"
        const val COL_OVERVIEW = "overview"
        const val COL_IMAGE = "image"
        const val COL_VOTE = "vote"

        val createTable = "CREATE TABLE " + TABLE_NAME + " (" +
                COL_IDMOVIE + " INTEGER PRIMARY KEY ," +
                COL_NAME + " VARCHAR(256)," +
                COL_DATE + " VARCHAR(256)," +
                COL_OVERVIEW + " VARCHAR(256)," +
                COL_IMAGE + " VARCHAR(256)," +
                COL_VOTE + " DOUBLE)"

        val selectData = "SELECT  * FROM $TABLE_NAME"
        val selectFav = "SELECT  * FROM $TABLE_NAME WHERE $COL_IDMOVIE"


    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }


    fun insertFavMovie(favorite: MovieFavoriteModel) {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_NAME, favorite.movieName)
        cv.put(COL_DATE, favorite.movieDate)
        cv.put(COL_OVERVIEW, favorite.movieOverview)
        cv.put(COL_IMAGE, favorite.movieImage)
        cv.put(COL_IDMOVIE, favorite.idMovie)
        cv.put(COL_VOTE, favorite.movieVote)

        val result = db.insert(TABLE_NAME, null, cv)

        if (result == -1.toLong()) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
        }

    }

    fun readAllMovie(): MutableList<MovieFavoriteModel> {
        val list: MutableList<MovieFavoriteModel> = ArrayList()
        val db = this.readableDatabase
        val result = db.rawQuery(selectData, null)
        if (result.moveToNext()) {
            do {

                val moviefav = MovieFavoriteModel()
                moviefav.idMovie = result.getString(result.getColumnIndex(COL_IDMOVIE)).toInt()
                moviefav.movieName = result.getString(result.getColumnIndex(COL_NAME))
                moviefav.movieDate = result.getString(result.getColumnIndex(COL_DATE))
                moviefav.movieOverview = result.getString(result.getColumnIndex(COL_OVERVIEW))
                moviefav.movieImage = result.getString(result.getColumnIndex(COL_IMAGE))
                moviefav.movieVote = result.getString(result.getColumnIndex(COL_VOTE)).toDouble()
                list.add(moviefav)
            } while (result.moveToNext())
        }
        return list
    }

    fun deleteFavMovie(id : Int) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, COL_IDMOVIE + "=" + id, null)
        Toast.makeText(context, "delete", Toast.LENGTH_SHORT).show()
        db.close()
    }

    fun checkFavMoview() {
        val db = this.writableDatabase
        val cursor = db.rawQuery(selectFav, null)

    }

}