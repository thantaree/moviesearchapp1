package com.example.moviesearchapp.`interface`

import com.example.moviesearchapp.model.MovieFavoriteModel

interface FavoriteMovieInterface {

    fun setAdapterFav(modelFavMovieList: ArrayList<MovieFavoriteModel>)

}