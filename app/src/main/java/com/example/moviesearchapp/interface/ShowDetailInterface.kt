package com.example.moviesearchapp.`interface`

import com.example.moviesearchapp.model.MovieFavoriteModel

interface ShowDetailInterface {

    fun setDetailMovie()
    fun addFavoriteMovie(list: MutableList<MovieFavoriteModel>)

}