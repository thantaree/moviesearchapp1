package com.example.moviesearchapp.`interface`

import com.example.moviesearchapp.model.MovieDetailModel

interface MovieInterface {

    fun setAdapter(modelMovieList: ArrayList<MovieDetailModel>)

}