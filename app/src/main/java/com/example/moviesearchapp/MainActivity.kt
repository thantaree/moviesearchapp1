package com.example.moviesearchapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.example.moviesearchapp.`interface`.MainInterface
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    MainInterface {

    var pageID: Int = 1
    var nowQuery: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addMovieSearch()

    }

    override fun addMovieSearch() {

        val addArrayList = ArrayList<String>()
        val addArrayAdapter = ArrayAdapter(
            this, android.R.layout.simple_list_item_1, addArrayList
        )

        search_bar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                nowQuery = query.toString()

                if(!addArrayList.contains(nowQuery)){
                    addArrayList.add(nowQuery)
                }

                addArrayList.reverse()
                rvAdd.adapter = addArrayAdapter
                search_bar.setQuery("", false)

                val intent = Intent(this@MainActivity, MovieActivity::class.java).apply {
                    putExtra("senddata", query)
                    putExtra("pageID", pageID)
                }
                startActivity(intent)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        rvAdd.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this@MainActivity, MovieActivity::class.java).apply {
                putExtra("senddata", addArrayList[position])
            }
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id: Int = item.itemId
        if (id == R.id.menu_favorite) {

            val intent = Intent(this, FavoriteActivity::class.java)
            startActivity(intent)

            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

