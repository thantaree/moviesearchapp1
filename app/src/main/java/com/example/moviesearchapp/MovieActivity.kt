package com.example.moviesearchapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesearchapp.`interface`.LoadmoreInterface
import com.example.moviesearchapp.adapter.MovieAdapter
import com.example.moviesearchapp.adapter.MovieClickListener
import com.example.moviesearchapp.`interface`.MovieInterface
import com.example.moviesearchapp.databaseFavorite.DatabaseHandler
import com.example.moviesearchapp.model.MovieDetailModel
import com.example.moviesearchapp.model.MovieFavoriteModel
import com.example.moviesearchapp.presenter.MoviePresenter
import com.example.moviesearchapp.service.MovieApiService
import com.example.moviesearchapp.service.MovieManager
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_movie.*
import kotlinx.android.synthetic.main.loading_layout.*

class MovieActivity : AppCompatActivity(), MovieInterface {

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var layoutManager: LinearLayoutManager
    var isLoading = false
    var query: String? = ""
    var pageID: Int = 1


    private val presenter = MoviePresenter(this, MovieManager().createService())

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        query = intent.getStringExtra("senddata")
        pageID = intent.getIntExtra("pageID", 1)

        actionBar()
        layoutManager = LinearLayoutManager(this)

        setMovieAdapter()
        presenter.getMovieApi(query, pageID)

    }


    fun actionBar() {
        val actionbar = supportActionBar
        actionbar?.let {
            title = ""
            actionbar.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    fun setMovieAdapter() {
        val listener = object : MovieClickListener {
            override fun onItemClick(
                imageURL: String,
                textName: String,
                vote: Double,
                textOverview: String,
                textDate: String,
                idMovie: Int

            ) {
                val intent = Intent(this@MovieActivity, ShowDetailActivity::class.java).apply {
                    putExtra("imageMovie", imageURL)
                    putExtra("textName", textName)
                    putExtra("vote", vote)
                    putExtra("textOverview", textOverview)
                    putExtra("textDate", textDate)
                    putExtra("idMovie", idMovie)
                }
                startActivity(intent)
            }
        }

        movieAdapter = MovieAdapter(listener)

        rvMovie.adapter = movieAdapter
        rvMovie.layoutManager = layoutManager
        rvMovie.itemAnimator = DefaultItemAnimator()
    }

    override fun setAdapter(modelMovieList: ArrayList<MovieDetailModel>) {

        movieAdapter.addList(modelMovieList)


        rvMovie.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                val visibleItemCount = layoutManager.childCount
                val pastVisibleItem = layoutManager.findFirstVisibleItemPosition()
                val total = movieAdapter.itemCount

                if (!isLoading) {
                    if ((visibleItemCount + pastVisibleItem) >= total) {
                        pageID++
                        presenter.getMovieApi(query, pageID)
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


}

