package com.example.moviesearchapp.service

import com.example.moviesearchapp.model.MovieModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieApiService {

    @Headers("api-key:eef52ef404343a9f4628d420e4e08e062e613720")
    @GET("/api/movies/search")

    fun getMovie(
        @Query("query") query: String,
        @Query("page") page: Int

    ): Call<MovieModel>
}