package com.example.moviesearchapp.service


import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieManager {
    companion object {
        const val BASE_MOVIE_API = "https://scb-movies-api.herokuapp.com"
    }

    fun createService(): MovieApiService =
        Retrofit.Builder()
            .baseUrl(BASE_MOVIE_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .run { create(MovieApiService::class.java) }
}